/*------------------------------------------------------------------------
File        :   Core.FileSys.FileSystem
Purpose     :   Core functionality for working with a filesystem 

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.
 
  ----------------------------------------------------------------------*/

/***************************************************************************/
/* Class Using                          */

USING Progress.Lang.*                   FROM PROPATH.
USING core.filesys.*                    FROM PROPATH.

/***************************************************************************/
/* Error Handling                       */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS core.filesys.FileSystem:
    
/***************************************************************************/
/* Class Properties                     */

DEFINE PUBLIC PROPERTY DirectorySeperator   AS CHARACTER NO-UNDO GET. PRIVATE SET.  

/****************************************************************************/
CONSTRUCTOR                             FileSystem():
/****************************************************************************/

CASE OPSYS:
WHEN "win32"    THEN 
    THIS-OBJECT:SetStandardSeperator("\").
WHEN "unix"     THEN
    THIS-OBJECT:SetStandardSeperator("/").
OTHERWISE 
    UNDO, THROW NEW Progress.Lang.AppError(SUBSTITUTE("FileSystem: Unknown operating system &1", QUOTER(OPSYS))).
END CASE.
 
END CONSTRUCTOR.

/****************************************************************************/
CONSTRUCTOR                             FileSystem(chDirPathSeperator   AS CHARACTER):
/****************************************************************************/
THIS-OBJECT:SetStandardSeperator(chDirPathSeperator).
END CONSTRUCTOR.

/****************************************************************************/
METHOD PRIVATE VOID                     SetStandardSeperator(chDirPathSeperator AS CHARACTER):
/****************************************************************************/
ASSIGN 
    THIS-OBJECT:DirectorySeperator = chDirPathSeperator
    .
END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GetStandardDirectoryFilePath(chPath AS CHARACTER):
/****************************************************************************/
/* Given a file path make sure its directory seperators are all             */
/* standardized                                                             */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chRetval                AS CHARACTER NO-UNDO.

/****************************************************************************/
CASE THIS-OBJECT:DirectorySeperator:
WHEN "~/" THEN 
    ASSIGN  
        chRetval = REPLACE(chPath, "~\", THIS-OBJECT:DirectorySeperator)
        .   
WHEN "~\" THEN
    ASSIGN  
        chRetval = REPLACE(chPath, "~/", THIS-OBJECT:DirectorySeperator)
        .  
END CASE. 

RETURN(chRetval).
END METHOD.

/****************************************************************************/
METHOD PUBLIC LOGICAL                   CreateDirectoryPath(chTargetDirectoryPath AS CHARACTER):
/****************************************************************************/
/* Make sure the specified directory path exists                            */
/* and create it if it doesn't                                              */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE iCnt                    AS INTEGER      NO-UNDO.
DEFINE VARIABLE iEntries                AS INTEGER      NO-UNDO.
DEFINE VARIABLE chEntry                 AS CHARACTER    NO-UNDO.
DEFINE VARIABLE chPath                  AS CHARACTER    NO-UNDO.

/****************************************************************************/

ASSIGN 
    chTargetDirectoryPath = TRIM(   THIS-OBJECT:GetStandardDirectoryFilePath(chTargetDirectoryPath), 
                                    THIS-OBJECT:DirectorySeperator
                                )
    .

ASSIGN 
    iEntries = NUM-ENTRIES( chTargetDirectoryPath, 
                            THIS-OBJECT:DirectorySeperator
                            )
    .
    
DO iCnt = 1 TO iEntries:

        /* Extract the directory path segment   */

    ASSIGN 
        chEntry = ENTRY(iCnt, 
                        chTargetDirectoryPath, 
                        THIS-OBJECT:DirectorySeperator
                        )
        
            /* And add it to the            */
            /* directory path known so far  */

        chPath  = chPath + THIS-OBJECT:DirectorySeperator WHEN chPath > "" 
        chPath  = chPath + chEntry
        
        FILE-INFO:FILE-NAME = chPath.
        .

        /* If the path exists - check what it is    */
        
    IF FILE-INFO:FILE-TYPE <> ? THEN 
        DO:
        IF SUBSTRING(FILE-INFO:FILE-TYPE, 1, 1) = "D" THEN
            NEXT.
  
        UNDO, THROW NEW Progress.Lang.AppError(SUBSTITUTE(  "Create directory path &1 contains a non-directory element at &2", 
                                                            QUOTER(chTargetDirectoryPath), 
                                                            QUOTER(chPath)
                                                        )
                                                ).
        END. 

        /* Create the directory     */

    OS-CREATE-DIR VALUE(chPath).

        /* Make sure it worked      */

    IF OS-ERROR > 0 THEN 
        UNDO, THROW NEW Progress.Lang.AppError(SUBSTITUTE(  "Create directory path &1 encountered error at &2", 
                                                            QUOTER(chTargetDirectoryPath), 
                                                            QUOTER(chPath)
                                                        )
                                                ).
END.

RETURN(TRUE).
END METHOD.

/****************************************************************************/
METHOD PUBLIC LOGICAL                   DeleteDirectoryPath(chTargetDirectoryPath AS CHARACTER):
/****************************************************************************/
/* Delete the target directory and everything under it                      */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE isTmp                   AS LOGICAL NO-UNDO.

/****************************************************************************/

ASSIGN 
    chTargetDirectoryPath   = TRIM(THIS-OBJECT:GetStandardDirectoryFilePath(chTargetDirectoryPath), 
                                                                            THIS-OBJECT:DirectorySeperator
                                    )
    isTmp                   = YES 
    .

    /* Does the path exist?         */
    
IF THIS-OBJECT:CheckPathExists(chTargetDirectoryPath)  THEN
    DO:
        
        /* If so, then delete it    */

    OS-DELETE VALUE(chTargetDirectoryPath).
    
        /* Track if this worked     */
        
    ASSIGN 
        isTmp = (OS-ERROR = 0)
        .
    END.

RETURN(isTmp).
END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 CombineDirectoryPathList(chPathList AS CHARACTER):
/****************************************************************************/
/* Combine a comma-delimited list of paths into one path. The last element  */
/* can point to the target file of the path                                 */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE iCnt                    AS INTEGER      NO-UNDO.
DEFINE VARIABLE iEntries                AS INTEGER      NO-UNDO.
DEFINE VARIABLE chCombinedPath          AS CHARACTER    NO-UNDO.
DEFINE VARIABLE chTmp                   AS CHARACTER    NO-UNDO.
DEFINE VARIABLE chEntry                 AS CHARACTER    NO-UNDO.

/****************************************************************************/

ASSIGN 
    chPathList  = THIS-OBJECT:GetStandardDirectoryFilePath(chPathList)
    iEntries    = NUM-ENTRIES(chPathList)
    .

DO iCnt = 1 TO iEntries:

    ASSIGN 
        chEntry = TRIM(ENTRY(iCnt, chPathList), THIS-OBJECT:DirectorySeperator)
        .
        
    IF LENGTH(chEntry) = 0 THEN 
        NEXT.
        
    ASSIGN 
        chCombinedPath  = chCombinedPath + THIS-OBJECT:DirectorySeperator WHEN chCombinedPath > "" 
        chCombinedPath  = chCombinedPath + chEntry
        .

END. 

RETURN(chCombinedPath).
END METHOD.

/****************************************************************************/
METHOD PUBLIC LOGICAL                   isDirectory(chDirectoryPath  AS CHARACTER):
/****************************************************************************/
/* Does the passed in directory path actually point to a directory?         */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chTmp                   AS CHARACTER    NO-UNDO.
DEFINE VARIABLE isTmp                   AS LOGICAL      NO-UNDO.

/***************************************************************************/
/* Initialize                           */

ASSIGN  
    chTmp               = FILE-INFO:FILE-NAME
    FILE-INFO:FILE-NAME = chDirectoryPath
    isTmp               = NO 
    .
    
/***************************************************************************/
/* Do Work                              */

WorkBlock:
DO:

IF FILE-INFO:TYPE = ? THEN 
    LEAVE.
    
ASSIGN 
    isTmp = (FILE-INFO:FILE-TYPE BEGINS "D")
    . 
    
END.

    /* put the name back    */
    
FILE-INFO:FILE-NAME = chTmp.

RETURN(isTmp).
END METHOD.

/****************************************************************************/
METHOD PUBLIC LOGICAL                   CheckPathExists(chTargetPath AS CHARACTER):
/****************************************************************************/
/* Check if there's something at the end of this pathing specification      */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chTmp                   AS CHARACTER    NO-UNDO.
DEFINE VARIABLE isTmp                   AS LOGICAL      NO-UNDO.

/***************************************************************************/

ASSIGN  
    chTmp               = FILE-INFO:FILE-NAME
    FILE-INFO:FILE-NAME = chTargetPath
    .

ASSIGN 
    isTmp = (FILE-INFO:TYPE <> ?)
    .
    
    /* put the name back    */
    
FILE-INFO:FILE-NAME = chTmp.

RETURN(isTmp).
END METHOD.

END CLASS.