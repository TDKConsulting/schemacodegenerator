/*------------------------------------------------------------------------
File        :   Code.GenerateTT.cls
Purpose     :   Generate a Temp Table definition corresponding to a table 
                schema 

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  

Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */		

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateTTDefinition
    INHERITS GenerateBase
    :

/****************************************************************************/
/* Class Buffer                         */

DEFINE BUFFER DbTable                   FOR DICTDB._file.

/****************************************************************************/
CONSTRUCTOR                             GenerateTTDefinition(   oGenSupport     AS GenerateSupport,
                                                                chTableName     AS CHARACTER 
                                                                ):
/****************************************************************************/
/* Configure GenerateBase - we're not going to use its generation           */
/* capabilities so the instance type doesn't matter                         */  
/****************************************************************************/
SUPER(  oGenSupport, 
        chTableName, 
        "CLASS"
        ).

FIND DbTable
    WHERE DbTable._file-name = chTableName
    NO-LOCK
    NO-ERROR
    .
    
END CONSTRUCTOR.

/****************************************************************************/
METHOD PUBLIC VOID                      GenerateTTDefinitionFile():
/****************************************************************************/
/* API to generate the TT definition file                                   */    
/****************************************************************************/

    /* Ouptut to "tt" + TableName + ".tt"   */
    
OUTPUT 
    TO VALUE(THIS-OBJECT:GetFullTablePathFileName("tt", "", "tt"))
    .
    
    /* Does this table have any fields?     */
    /* Generate the TT if so                */
        
IF THIS-OBJECT:CheckIfThisTableHasAnyFields(RECID(DbTable)) THEN
    DO:  
    THIS-OBJECT:GenerateHeader().
    THIS-OBJECT:GenerateFields().
    THIS-OBJECT:GenerateIndexes().
    THIS-OBJECT:GenerateFooter().
    END.
ELSE        /* No fields in the table -> generate a comment to that effect  */ 
    DO:
    
    PUT UNFORMATTED
        THIS-OBJECT:GenerateCommentTableHasNoFields() SKIP 
        .
    
    END.
OUTPUT CLOSE.

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateHeader():
/****************************************************************************/
/* Generate the header of the TT definition file                            */    
/****************************************************************************/
/* Local Variables                      */

/****************************************************************************/

PUT UNFORMATTED
        /* Generate &GLOBAL-DEFINE sentinel header  */
        
    THIS-OBJECT:GetGlobalDefineHeader("tt", "", "tt")                                       SKIP(1)
    
        /* Generate the TT definition header        */
        
    "DEFINE TEMP-TABLE tt" + THIS-OBJECT:TableName + " NO-UNDO"                             SKIP 
    SPACE(THIS-OBJECT:GenerateSupport:TabSize)  "BEFORE-TABLE bt" + THIS-OBJECT:TableName   SKIP
    .

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateFields():
/****************************************************************************/
/* Generate all the fields for the TT that map to the db table              */    
/****************************************************************************/
/* Local Buffer                         */

DEFINE BUFFER TableField                FOR DICTDB._field.

/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chFieldLine             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE isCommentLine           AS LOGICAL      NO-UNDO.
DEFINE VARIABLE chTabSizeFmt            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE iDataTypeLength         AS INTEGER      NO-UNDO.
DEFINE VARIABLE chTmp                   AS CHARACTER    NO-UNDO.
DEFINE VARIABLE isSkip                  AS LOGICAL      NO-UNDO.

/***************************************************************************/
/* Initialize                           */

    /* Standard format for a single tab stop    */

ASSIGN 
    chTabSizeFmt = "X(" + STRING(THIS-OBJECT:GenerateSupport:TabSize) + ")"
    .
    
/***************************************************************************/
/* Do Work                              */

    /* Process each field in a table    */
    
FOR EACH TableField
    WHERE TableField._file-recid = RECID(DbTable)
    NO-LOCK
    BY TableField._order 
    :

        /* Can't have RAW field in a TT -                   */
        /* this will add the field with it commented out    */
    
    ASSIGN 
        isCommentLine   = LOOKUP(TableField._Data-type, "RAW") > 0
        isSkip          = NO 
        .
        
        /* Form the field definition    */

    ASSIGN 
        chFieldLine     =   STRING(IF isCommentLine 
                                    THEN "~/~*" 
                                    ELSE "",                           chTabSizeFmt)    + 
                            STRING("FIELD " + TableField._Field-name,       "X(38)")    +  
                            STRING(" AS "   + UPPER(TableField._Data-Type), "X(15)")
        .
        
        /* Remember the datatype length for this field.     */
        /* This is used to column-align successive lines    */
        /* of code                                          */
    
    ASSIGN 
        iDataTypeLength = LENGTH(chFieldLine)
        .
        
        /* Write the initial field definition       */
        
    PUT UNFORMATTED 
        chFieldLine
        .
        
        /* Add code for the field's initial value   */

    IF TableField._Initial > "" THEN 
        DO:
        PUT UNFORMATTED      
            IF LOOKUP(TableField._data-type, "CHARACTER,LOGICAL") > 0  
                THEN THIS-OBJECT:GenerateQuotedAttribute(   TableField._Initial, 15, "INITIAL")
                ELSE THIS-OBJECT:GenerateAttribute(         TableField._Initial, 15, "INITIAL")
            SKIP
            SPACE(iDataTypeLength) 
            .
            
        ASSIGN 
            isSkip = YES 
            .

        END.
        
        /* Write code for the field extents         */

    ASSIGN 
        chTmp   = THIS-OBJECT:GenerateAttribute(TableField._Extent, 3,      "EXTENT")     
        .
        
    IF chTmp  > "" THEN
        DO: 
        PUT UNFORMATTED             
            chTmp       SKIP 
            SPACE(iDataTypeLength)
            .
            
        ASSIGN 
            isSkip = YES 
            .
        END.

        /* Generate the remainder of the field code for     */
        /* FORMAT, LABEL, and COLUMN-LABEL settings         */

    PUT UNFORMATTED
        THIS-OBJECT:GenerateQuotedAttribute(TableField._Format,     15, "FORMAT")           + 
        THIS-OBJECT:GenerateQuotedAttribute(TableField._Label,      15, "LABEL")            + 
        THIS-OBJECT:GenerateQuotedAttribute(TableField._Col-label,  20, "COLUMN-LABEL")     + 

            /* Add closing comment if required              */
 
        (IF isCommentLine THEN "~*~/" ELSE "")                                                
        
        SKIP 
        .

END.

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateIndexes():
/****************************************************************************/
/* Generate code to replicate all the indexes in a DB table                 */
/****************************************************************************/
/* Local Buffer                         */

DEFINE BUFFER TableIndex                FOR dictdb._Index.
DEFINE BUFFER TableField                FOR dictdb._Field.
DEFINE BUFFER IndexField                FOR dictdb._Index-Field.

/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE isPrimary               AS LOGICAL      NO-UNDO.
DEFINE VARIABLE isUnique                AS LOGICAL      NO-UNDO.
DEFINE VARIABLE chLineStart             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE iPrefixLength           AS INTEGER      NO-UNDO.
DEFINE VARIABLE chPrefixFormat          AS CHARACTER    NO-UNDO.

DEFINE VARIABLE chNameFmt               AS CHARACTER    NO-UNDO.

/****************************************************************************/

    /* Process all indexes in the table */

FOR EACH TableIndex
    WHERE TableIndex._File-recid = RECID(DbTable)
    NO-LOCK
    :

        /* Get some index configuration settings    */
        
    ASSIGN 
        isPrimary   = (RECID(TableIndex) = DbTable._prime-index  )
        isUnique    = TableIndex._unique

            /* Format for index names is 20 chars minimum   */

        chNameFmt   = "X(" + STRING(MAX(20, LENGTH(TableIndex._index-name) + 1)) + ")"
        .

        /* Start of the INDEX line is one tab stop in   */
        /* then the index name                          */
        /* then the primary and unique index settings   */
        /* padded to 20 chars length                    */

    ASSIGN 
        chLineStart =   FILL(" ", THIS-OBJECT:GenerateSupport:TabSize)                  +     
                        "INDEX " + STRING(TableIndex._index-name, chNameFmt)            +
                        STRING(IF isPrimary OR 
                                  isUnique 
                                THEN " IS " + (IF isPrimary THEN "PRIMARY " ELSE "")    +
                                              (IF isUnique  THEN "UNIQUE "  ELSE "")   
                                ELSE "", 
                                "X(20)")    
                                
        iPrefixLength   = LENGTH(chLineStart) 
        chPrefixFormat  = "X(" + STRING(MAX(iPrefixLength, 44)) + ")"
        .

        /* Process each field in the index              */

    FOR EACH IndexField
        WHERE IndexField._index-recid = RECID(TableIndex)
        NO-LOCK, 
        
        EACH TableField 
            WHERE RECID(TableField) = IndexField._field-recid 
            NO-LOCK
            
        BREAK BY IndexField._index-recid
              BY IndexField._index-seq
              :

            /* Don't write index specs for      */
            /* tables with only a default index */

        IF FIRST(IndexField._index-recid)   AND 
           TableField._field-name > ""      THEN 
            PUT UNFORMATTED                             SKIP(1)
                STRING(chLineStart, chPrefixFormat) 
                .

            /* Write the index field            */

        PUT UNFORMATTED
            IF FIRST-OF(IndexField._index-recid) 
                THEN    TableField._field-name          /* If this is the first name just write it out      */      
                ELSE    STRING(" ", chPrefixFormat) +   /* for 2nd and successfield fields add the leading  */
                        TableField._field-name          /* spaces to line up the index field specifiation   */
            SKIP 
            .

    END.

END.

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateFooter():
/****************************************************************************/
/* Finish generating the TT with the trailing period and the ending         */
/* &ENDIF for the &GLOBAL-DEFINE sentinel                                   */     
/****************************************************************************/

PUT UNFORMATTED                                         SKIP 
    SPACE(THIS-OBJECT:GenerateSupport:TabSize)     "."  SKIP(1)
    "&ENDIF"                                            SKIP 
    .
    
END METHOD.

END CLASS.