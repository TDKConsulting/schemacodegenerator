/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateDomainClass.cls
Purpose     :   Generate a domain object class which is a class of properties 
                corresponding to the fields in a db table. This class only 
                holds values and does not pass the read / write action to 
                a TT or DB table field.
                
                This is distinct from GenerateIndirectionClassBase which 
                generates code to propogate get / setting between the property 
                and a corresponding db / tt field. 
                   
Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING core.filesys.*                    FROM PROPATH.
USING Generate.Code.* FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */		

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateDomainClass
    INHERITS GenerateBase
    :

/****************************************************************************/
/* Class Buffer                         */

DEFINE BUFFER DbTable                   FOR DICTDB._file.

/****************************************************************************/
CONSTRUCTOR                             GenerateDomainClass(oGenSupport     AS GenerateSupport,
                                                            chTableName     AS CHARACTER 
                                                            ):
/****************************************************************************/
SUPER(  oGenSupport, 
        chTableName, 
        "CLASS"
        ).

FIND DbTable
    WHERE DbTable._file-name = chTableName   
    NO-LOCK
    NO-ERROR
    .
    
END CONSTRUCTOR.

/****************************************************************************/
METHOD PUBLIC VOID                      GenerateDomainClassFile():
/****************************************************************************/

OUTPUT 
    TO VALUE(THIS-OBJECT:GetFullTablePathFileName("", "", "cls"))
    .

THIS-OBJECT:GenerateHeader().

IF THIS-OBJECT:CheckIfThisTableHasAnyFields(RECID(DbTable)) THEN 
    DO:    
    THIS-OBJECT:GenerateProperties().
    END.
ELSE 
    DO:
    PUT UNFORMATTED
        THIS-OBJECT:GenerateCommentTableHasNoFields() SKIP  
        .
    END.

THIS-OBJECT:GenerateFooter().
OUTPUT CLOSE.

END METHOD.

/***************************************************************************/
METHOD PRIVATE VOID                     GenerateHeader():
/***************************************************************************/
SUPER:GenerateClassHeader("", "", "").
END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateProperties():
/****************************************************************************/
/* Local Buffer                         */

DEFINE BUFFER TableField                FOR DICTDB._field.

/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chPrefix                AS CHARACTER NO-UNDO.
DEFINE VARIABLE chSuffix                AS CHARACTER NO-UNDO.

/****************************************************************************/

FOR EACH TableField
    WHERE TableField._file-recid = RECID(DbTable)
    NO-LOCK
    BY TableField._order 
    :

    IF LOOKUP(TableField._Data-Type, "RAW,CLOB") > 0 THEN 
        ASSIGN  
            chPrefix    = "&IF FALSE &THEN"
            chSuffix    = "&ENDIF"
            .
    ELSE 
        ASSIGN  
            chPrefix    = ""
            chSuffix    = ""
            .            

    PUT UNFORMATTED
        chPrefix            SKIP 
        "DEFINE PUBLIC PROPERTY "   + STRING(TableField._Field-name,        "X(32)") + 
                            " AS "  + STRING(UPPER(TableField._Data-Type),  "X(15)") + " NO-UNDO"
                            
                            (IF TableField._Extent > 0 
                                THEN " EXTENT " + STRING(TableField._Extent) 
                                ELSE ""
                                )  
                                
                            " GET. SET."  
                            SKIP
        chSuffix            SKIP
        .
         
END.

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateFooter():
/****************************************************************************/
/* Local Buffer                         */

PUT UNFORMATTED                                 SKIP(1)
    SUBSTITUTE("END &1.", THIS-OBJECT:FileType) SKIP 
    .

END METHOD.

END CLASS.