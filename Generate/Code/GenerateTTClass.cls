/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateTTClass
Purpose     :   Generate the code for a TT indirection object, with a 
                replacement GenerateHeader() API to generate the class 
                header and the include file for the PDS file.   
                    
Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateTTClass
    INHERITS GenerateIndirectionClassBase
    : 

/****************************************************************************/
CONSTRUCTOR                             GenerateTTClass(oGenSupport     AS GenerateSupport,
                                                        chTableName     AS CHARACTER
                                                        ):
/****************************************************************************/
/* Configure GenerateIndirectionClassBase for a TT class                    */
/****************************************************************************/
SUPER(  oGenSupport,        /* General support object       */ 
        chTableName,        /* Table we're generating for   */ 
        "CLASS",            /* This is a class file         */
        "pds",              /* Class name prefix            */
        "tt"                /* Table name prefix to match the TT definition */
        ).

END CONSTRUCTOR.

/****************************************************************************/
METHOD PUBLIC VOID                      GenerateTTClassFile():
/****************************************************************************/
/* API to generate the TT class file                                        */    
/****************************************************************************/
THIS-OBJECT:GenerateClassFile().
END METHOD.

/***************************************************************************/
METHOD PROTECTED OVERRIDE VOID          GenerateHeader():
/****************************************************************************/
/* The header of this class is different from a stock class header in that  */
/* this class needs a TT definition include added. To do this the code will */
/* override GenerateIndirectionClassBase:GenerateHeader() and add the       */
/* extra code after the stock header has been written.                      */
/***************************************************************************/

    /* Generate the CLASS header text               */

SUPER:GenerateHeader().

    /* Generate the dataset include file reference  */
    
PUT UNFORMATTED
    "~{"        + THIS-OBJECT:GetOffsetTablePathFileName(THIS-OBJECT:ClassNamePrefix, "", "pds") + "~}"    SKIP(1)
    .

END METHOD.

END CLASS.