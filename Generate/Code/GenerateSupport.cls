/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateSupport
Purpose     :   Holds configuration values for the current db table 

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING core.filesys.* FROM PROPATH.
USING Generate.Code.* FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */		

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateSupport
    INHERITS FileSystem 
    :
    
/****************************************************************************/
/* Class Properties                     */

    /* This is the base directory to put all generated code */
    
DEFINE PUBLIC PROPERTY BaseDirectory        AS CHARACTER    NO-UNDO GET. PRIVATE SET.

    /* This is the offset from the base directory           */
    /* to the current target directory                      */ 
 
DEFINE PUBLIC PROPERTY DirectoryOffset      AS CHARACTER    NO-UNDO GET. PRIVATE SET.

    /* This is a combination of BaseDirectory + DirectoryOffset     */
    /* which gives the full directory path                          */
     
DEFINE PUBLIC PROPERTY FullDirectoryPath    AS CHARACTER    NO-UNDO GET. PRIVATE SET.

    /* Size of each tab stop in the code                    */
     
DEFINE PUBLIC PROPERTY TabSize              AS INTEGER      NO-UNDO GET. PRIVATE SET.  

/****************************************************************************/
CONSTRUCTOR                             GenerateSupport(chBaseDir   AS CHARACTER, 
                                                        chDirOffset AS CHARACTER
                                                        ):
/****************************************************************************/
/* Initialize the base directory, current directory offset, and             */
/* a tab stop size of 4 spaces                                              */
/****************************************************************************/
THIS-OBJECT(chBaseDir, 
            chDirOffset, 
            4
            ).
END CONSTRUCTOR.

/****************************************************************************/
CONSTRUCTOR                             GenerateSupport(chBaseDir   AS CHARACTER, 
                                                        chDirOffset AS CHARACTER, 
                                                        iTabSize    AS INTEGER 
                                                        ):
/****************************************************************************/
/* Initialize the base directory, current directory offset, and             */
/* a tab stop size                                                          */
/****************************************************************************/
SUPER().        /* Make sure the base class is setup first  */

ASSIGN 
    THIS-OBJECT:BaseDirectory       = THIS-OBJECT:GetStandardDirectoryFilePath(chBaseDir)
    THIS-OBJECT:DirectoryOffset     = THIS-OBJECT:GetStandardDirectoryFilePath(chDirOffset)
    THIS-OBJECT:FullDirectoryPath   = THIS-OBJECT:CombineDirectoryPathList(THIS-OBJECT:BaseDirectory + "," + THIS-OBJECT:DirectoryOffset)
    THIS-OBJECT:TabSize             = iTabSize
    .

END CONSTRUCTOR.

END CLASS.