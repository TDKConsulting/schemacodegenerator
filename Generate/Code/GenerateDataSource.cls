/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateDataSource.cls
Purpose     :   Generate data source code for a given table  

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */		

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateDataSource
    INHERITS GenerateBase
    :

/****************************************************************************/
/* Class Buffer                         */

DEFINE BUFFER DbTable                   FOR DICTDB._file.

/****************************************************************************/
CONSTRUCTOR                             GenerateDataSource( oGenSupport     AS GenerateSupport,
                                                            chTable         AS CHARACTER 
                                                            ):
/****************************************************************************/
/* Initialize configuration for this table as a data source                 */                                                                
/****************************************************************************/
SUPER(  oGenSupport, 
        chTable,
        "DATASOURCE"
        ).

FIND DbTable
    WHERE DbTable._file-name = chTable
    NO-LOCK
    NO-ERROR
    .
    
END CONSTRUCTOR.

/****************************************************************************/
METHOD PUBLIC VOID                      GenerateDataSourceFile():
/****************************************************************************/
/* Write all the datasource code                                            */ 
/****************************************************************************/

    /* Where to send the datasource code    */
    
OUTPUT 
    TO VALUE(THIS-OBJECT:GetFullTablePathFileName("dsrc", "", "i"))
    .
    
    /* Write the code                       */
    
THIS-OBJECT:GenerateHeader().
THIS-OBJECT:GenerateBody().
THIS-OBJECT:GenerateFooter().

    /* All done!                            */
    
OUTPUT CLOSE.

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateHeader():
/****************************************************************************/
/* Write the top part of the file                                           */    
/****************************************************************************/

PUT UNFORMATTED
    THIS-OBJECT:GetGlobalDefineHeader("dsrc", "", "i")                      SKIP(1)
    .

END METHOD.
                                                                            
/****************************************************************************/
METHOD PRIVATE VOID                     GenerateBody():
/****************************************************************************/
/* Write the body of the code                                               */    
/****************************************************************************/

PUT UNFORMATTED
    SUBSTITUTE("DEFINE DATA-SOURCE dsrc&1 FOR &1.", THIS-OBJECT:TableName)   SKIP
    .

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateFooter():
/****************************************************************************/
/* Write the last part of the code                                          */    
/****************************************************************************/

PUT UNFORMATTED                                         SKIP 
    "&ENDIF"                                            SKIP 
    .
    
END METHOD.

END CLASS.