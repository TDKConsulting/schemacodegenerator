/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateIndirectionClassBase
Purpose     :   Generate code that propogates get / set actions between a 
                property and its corresponding DB / TT field. 
                
                This is distinct from the domain object class which only holds 
                values in its properties and does not copy values to / from a 
                tt / db field.   
Syntax      : 
Description : 
Notes       : 
Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  

Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */		

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateIndirectionClassBase
    INHERITS GenerateBase
    :

/****************************************************************************/
/* Class Buffer                         */

DEFINE BUFFER DbTable                   FOR DICTDB._file.

/****************************************************************************/
/* Class Properties                     */

    /* Class name prefix - "db", "i", etc.  */

DEFINE PUBLIC PROPERTY ClassNamePrefix  AS CHARACTER NO-UNDO GET. PRIVATE SET.

    /* Fully qualified name of the class    */
    
DEFINE PUBLIC PROPERTY ClassName        AS CHARACTER NO-UNDO GET. PRIVATE SET.

    /* Prefix to put before the table name  */
    
DEFINE PUBLIC PROPERTY TableNamePrefix  AS CHARACTER NO-UNDO GET. PRIVATE SET.
 
/****************************************************************************/
CONSTRUCTOR  PROTECTED                  GenerateIndirectionClassBase(   oGenSupport     AS GenerateSupport,
                                                                        chTableName     AS CHARACTER,
                                                                        chFileType      AS CHARACTER, 
                                                                        chClassPrefix   AS CHARACTER, 
                                                                        chTablePrefix   AS CHARACTER 
                                                                        ):
/****************************************************************************/

    /* Configure GenerateBase with the table and            */
    /* type of file we're working on                        */

SUPER(  oGenSupport, 
        chTableName, 
        chFileType
        ).

    /* Get the target table _file record in scope           */

FIND DbTable
    WHERE DbTable._file-name = chTableName   
    NO-LOCK
    NO-ERROR
    .

    /* Establish the standard structure for a class name:   */
    /* Class Name Prefix + Table Name                       */  

ASSIGN
    THIS-OBJECT:ClassNamePrefix = chClassPrefix                 
    THIS-OBJECT:ClassName       = THIS-OBJECT:ClassNamePrefix   + THIS-OBJECT:TableName  
    THIS-OBJECT:TableNamePrefix = chTablePrefix  
    .      

END CONSTRUCTOR.

/****************************************************************************/
METHOD PROTECTED VOID                      GenerateClassFile():
/****************************************************************************/

    /* Write this to a .cls file                    */

OUTPUT 
    TO VALUE(THIS-OBJECT:GetFullTablePathFileName(THIS-OBJECT:ClassNamePrefix, "", "cls"))
    .

    /* Generate the top part of the class file      */
    
THIS-OBJECT:GenerateHeader().

    /* If this table has fields then generate properties for them   */
    /* If not, then generate a 'no fields here' comment             */

IF THIS-OBJECT:CheckIfThisTableHasAnyFields(RECID(DbTable)) THEN 
    DO:    
    THIS-OBJECT:GenerateProperties().
    END.
ELSE 
    DO:
    PUT UNFORMATTED
        THIS-OBJECT:GenerateCommentTableHasNoFields()   SKIP   
        .
    END.

    /* Generate the ending part of the class        */

THIS-OBJECT:GenerateFooter().
OUTPUT CLOSE.

END METHOD.

/****************************************************************************/
METHOD PROTECTED VOID                   GenerateHeader():
/****************************************************************************/
/* The header of the class is the package + class name                      */
/****************************************************************************/
SUPER:GenerateClassHeader(THIS-OBJECT:ClassNamePrefix, "", "").
   
END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateProperties():
/****************************************************************************/
/* Generate the properties to do the table.field to property mapping        */
/****************************************************************************/
/* Local Buffer                         */

DEFINE BUFFER TableField                FOR DICTDB._field.

/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chGet                   AS CHARACTER NO-UNDO.

DEFINE VARIABLE chTmpStr                AS CHARACTER NO-UNDO.
DEFINE VARIABLE chParmList              AS CHARACTER NO-UNDO.
DEFINE VARIABLE chTmpVar                AS CHARACTER NO-UNDO.
DEFINE VARIABLE chTableFieldName        AS CHARACTER NO-UNDO.
DEFINE VARIABLE chIndexVar              AS CHARACTER NO-UNDO.

DEFINE VARIABLE chPropertyPrefix        AS CHARACTER NO-UNDO.
DEFINE VARIABLE chPropertySuffix        AS CHARACTER NO-UNDO.

/****************************************************************************/

    /* Traverse all the fields for a table      */

FOR EACH TableField
    WHERE TableField._file-recid = RECID(DbTable)
    NO-LOCK
    BY TableField._order 
    :

        /* Can't do RAW and CLOB mappings so wrap them  */
        /* in code that'll effectively comment them out */

    IF LOOKUP(TableField._Data-Type, "RAW,CLOB") > 0 THEN 
        ASSIGN  
            chPropertyPrefix    = "&IF FALSE &THEN"
            chPropertySuffix    = "&ENDIF"
            .
    ELSE 
        ASSIGN 
            chPropertyPrefix    = ""
            chPropertySuffix    = ""
            .

        /* Create the reference to table & field    */
        /* TableNamePrefix is used for temp-tables  */
        /* ie "ttTableName"                         */

    ASSIGN 
        chTableFieldName    =   THIS-OBJECT:TableNamePrefix + 
                                DbTable._file-name          + "." + 
                                TableField._field-name 
        .

        /* Property name, datatype definition               */

    ASSIGN 
        chTmpStr = "DEFINE PUBLIC PROPERTY "    + STRING(TableField._Field-name,        "X(32)") + 
                   " AS "                       + STRING(UPPER(TableField._Data-Type),  "X(15)") + " NO-UNDO "
        .

        /* All mapping goes through the same variable name  */
        /* with a data type that matches the table field    */

    ASSIGN 
        chParmList  = "tmpvar AS " + UPPER(TableField._data-type) 
        chTmpVar    = "tmpvar"
        .

        /* If this field has an extent,                     */
        /* add that to the configuraton                     */
        
    IF TableField._Extent > 0 THEN 
        ASSIGN 
            chTmpStr            = chTmpStr + " EXTENT " + STRING(TableField._Extent)
            chIndexVar          = "ix AS INTEGER"                       /* Array element #  */
            chParmList          = chParmList + ", " + chIndexVar        /* Added to the parameter list. This is implictly   */
            chTableFieldName    = chTableFieldName + "[ix]"             /* set when doing Object:PropertyName[Number]       */    
            .

    ELSE 

        ASSIGN 
            chIndexVar  = ""
            .

        /* Write the property out                   */

    PUT UNFORMATTED
        
            /* &IF FALSE if needed                  */
        
        chPropertyPrefix                SKIP         
            
            /* The property structural declaration  */
        
        chTmpStr                                    
                                 
            /* All the GET / SET code               */

        SUBSTITUTE(THIS-OBJECT:GetPropertyGetSet(), 
                    chIndexVar, 
                    chTableFieldName, 
                    chParmList, 
                    chTmpVar
                    )                   SKIP

            /* &ENDIF if needed                     */

        chPropertySuffix                SKIP(1)          
        .

END.

END METHOD.

/****************************************************************************/
METHOD PROTECTED CHARACTER              GetPropertyGetSet():
/****************************************************************************/
/* Create a string of all the GET / SET code to do the table.field to       */
/* property mapping. CHR(10) gets turned into a CR/NL in the output.        */    
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chRetval                AS CHARACTER NO-UNDO.

/****************************************************************************/

ASSIGN
    chRetval =                                        CHR(10) + 
                FILL(" ", 75) + "GET(&1):"          + CHR(10) + 
                FILL(" ", 75) + "RETURN(&2)."       + CHR(10) + 
                FILL(" ", 75) + "END GET."          + CHR(10) +
                FILL(" ", 75) + "SET(&3):"          + CHR(10) +
                FILL(" ", 75) + "ASSIGN &2 = &4."   + CHR(10) +
                FILL(" ", 75) + "END SET."
    .
    
RETURN(chRetval).
END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateFooter():
/****************************************************************************/
/* Generate the trailing portion of the class                               */
/****************************************************************************/
/* Local Buffer                         */

PUT UNFORMATTED                                     SKIP(1)
    SUBSTITUTE("END &1.", THIS-OBJECT:FileType)     SKIP 
    .

END METHOD.
END CLASS.