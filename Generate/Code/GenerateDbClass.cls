/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateDbClass.cls
Purpose     :   Generate class which maps property get / set to a 
                corresponding db table field   

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateDbClass
    INHERITS GenerateIndirectionClassBase
    : 

/****************************************************************************/
CONSTRUCTOR                             GenerateDbClass(oGenSupport     AS GenerateSupport,
                                                        chTableName     AS CHARACTER
                                                        ):
/****************************************************************************/
/* Initialize this class a as CLASS with a "db" prefix to its name          */
/****************************************************************************/
SUPER(oGenSupport, chTableName, "CLASS", "db", "").

END CONSTRUCTOR.

/***************************************************************************/
METHOD PUBLIC VOID                      GenerateDbClassFile():
/****************************************************************************/
/* Map the "generate" API to the GenerateClassFile() in                     */
/* GenerateIndirectionClassBase                                             */     
/****************************************************************************/
THIS-OBJECT:GenerateClassFile().
END METHOD.

END CLASS.
