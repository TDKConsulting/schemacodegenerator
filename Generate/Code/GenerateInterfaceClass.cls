/*------------------------------------------------------------------------
File        :   Generate.Code.GenerateInterfaceClass
Purpose     :   Create the INTERFACE class declaration for all the 
                indirection objects  

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateInterfaceClass
    INHERITS GenerateIndirectionClassBase
    : 
    
/****************************************************************************/
CONSTRUCTOR                             GenerateInterfaceClass( oGenSupport     AS GenerateSupport, 
                                                                chTableName     AS CHARACTER
                                                                ):
/****************************************************************************/
/* Configure GenerateIndirectionClassBase for creating an interface class   */
/****************************************************************************/
SUPER(  oGenSupport,        /* Support object instance                          */
        chTableName,        /* Name of the table to generate the interface for  */ 
        "INTERFACE",        /* Class type                                       */ 
        "I",                /* Class name prefix                                */ 
        ""
        ).

END CONSTRUCTOR.

/****************************************************************************/
METHOD PUBLIC VOID                      GenerateInterfaceClassFile():
/****************************************************************************/
/* API to generate the interface file                                       */
/****************************************************************************/
THIS-OBJECT:GenerateClassFile().
END METHOD.

/****************************************************************************/
METHOD PROTECTED OVERRIDE CHARACTER     GetPropertyGetSet():
/****************************************************************************/
/* Interfaces don't have code in the GET / SET parts of the property specification. */
/* This API overrides GenerateIndirectionClassBase:GetPropertyGetSet()              */
/* to return what the interface requires                                            */    
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chRetval                AS CHARACTER NO-UNDO.

/****************************************************************************/

ASSIGN
    chRetval =  " GET. SET."
    .
    
RETURN(chRetval).
END METHOD.

END CLASS.
