/*------------------------------------------------------------------------
File        :   Code.GenerateBase.cls
Purpose     :   Base class corresponding to a db table the code is 
                being generated for. 
                
                This also holds the current table configuration 
                information as well as generic code used in different 
                aspects of the generation process.

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.
        
  ----------------------------------------------------------------------*/

/***************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/***************************************************************************/
/* Error Handling                       */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GenerateBase:

/****************************************************************************/
/* Class Properties                     */

    /* General support object instance                              */

DEFINE PUBLIC PROPERTY GenerateSupport  AS GenerateSupport  NO-UNDO GET. PRIVATE SET.

    /* What kind of file is being generated?                        */
    /* Should be or CLASS                                           */

DEFINE PUBLIC PROPERTY FileType         AS CHARACTER        NO-UNDO GET. PRIVATE SET.   

    /* Name of the db's table being generated                       */
    
DEFINE PUBLIC PROPERTY TableName        AS CHARACTER        NO-UNDO GET. PRIVATE SET.   

    /* Database and table part of a path to a file                  */
     
DEFINE PUBLIC PROPERTY OffsetTablePath  AS CHARACTER        NO-UNDO GET. PRIVATE SET.   

    /* Full path from the root directory to the target directory    */

DEFINE PUBLIC PROPERTY FullTablePath    AS CHARACTER        NO-UNDO GET. PRIVATE SET.    

/****************************************************************************/
CONSTRUCTOR                             GenerateBase(   oGenSupport     AS GenerateSupport, 
                                                        chTableName     AS CHARACTER, 
                                                        chFileType      AS CHARACTER 
                                                        ):
/****************************************************************************/
/* Initialize                           */

ASSIGN 
    THIS-OBJECT:GenerateSupport = oGenSupport
    THIS-OBJECT:TableName       = chTableName
    THIS-OBJECT:FileType        = chFileType

    THIS-OBJECT:FullTablePath   = THIS-OBJECT:GetFullTablePath()

    THIS-OBJECT:OffsetTablePath = THIS-OBJECT:GenerateSupport:CombineDirectoryPathList( THIS-OBJECT:GenerateSupport:DirectoryOffset + "," + 
                                                                                        THIS-OBJECT:TableName
                                                                                        ) 
    .

END CONSTRUCTOR.

/****************************************************************************/
METHOD PROTECTED VOID                     GenerateClassHeader(  chPrefix        AS CHARACTER, 
                                                                chSuffix        AS CHARACTER, 
                                                                chExtension     AS CHARACTER 
                                                                ):
/****************************************************************************/
/* generate code for a class or interface header                            */
/****************************************************************************/
    
PUT UNFORMATTED
    THIS-OBJECT:FileType  + " " + 
    REPLACE(THIS-OBJECT:GetOffsetTablePathFileName( chPrefix, 
                                                    chSuffix, 
                                                    chExtension),       /* Generate an offset directory / file name     */  
            THIS-OBJECT:GenerateSupport:DirectorySeperator,             /* the directory seperator we're using          */
            ".")                                                        /* and replace the seperator with periods       */      
    .                                                                   /* which is what's used in a class / interface  */

    /* If this is not an interface, then                */
    /* add the "implements" code                        */

IF THIS-OBJECT:FileType <> "interface" THEN 
    PUT UNFORMATTED                                     SKIP 
        FILL(" ", THIS-OBJECT:GenerateSupport:TabSize)                                      /* Indent one tab stop over                                 */
        "IMPLEMENTS " REPLACE(  THIS-OBJECT:GetOffsetTablePathFileName( "I",                /* Interface names are always preceded by an "I"            */
                                                                        chSuffix,           /* Suffix to the class name                                 */
                                                                        chExtension),       
                                THIS-OBJECT:GenerateSupport:DirectorySeperator,             /* Directory seperator                                      */
                                ".")                    SKIP                                /* turned into periods to match interface name structure    */
        
        .

    /* Write the end of the statement                   */
    
PUT UNFORMATTED
    FILL(" ", THIS-OBJECT:GenerateSupport:TabSize) ":"  SKIP(1)                             /* Indented one tab stop over                               */ 
    .

END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GenerateQuotedAttribute(chAttribute AS CHARACTER, 
                                                                iAttrWidth  AS INTEGER, 
                                                                chPrefix    AS CHARACTER
                                                                ):
/****************************************************************************/
/* Generate a right-space padded quoted attribute - this is used to get an  */
/* standard sized attribute which is used to column-align a set of code     */
/* elements. This version allows for attributes which are larger than the   */
/* expected attribute width                                                 */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chTmp                   AS CHARACTER NO-UNDO.

/****************************************************************************/

IF chAttribute > "" THEN
    ASSIGN 
        chTmp   =   " " + chPrefix + 
                    " " + STRING(   QUOTER(chAttribute), 
                                    "X(" + STRING(MAX(iAttrWidth, LENGTH(chAttribute) + 2)) + ")"
                                )  
        .

RETURN(chTmp). 
END METHOD. 

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GenerateAttribute(  chAttribute AS CHARACTER, 
                                                            iAttrWidth  AS INTEGER,
                                                            chPrefix    AS CHARACTER
                                                            ):
/****************************************************************************/
/* Generate a space-padded attribute - this is used to get an standard      */
/* sized attribute which is used to column-align a set of code elements.    */
/* This version allows for attributes which are larger than the expected    */
/* attribute width.                                                         */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chTmp                   AS CHARACTER NO-UNDO.

/****************************************************************************/

IF chAttribute > "" THEN
    ASSIGN 
        chTmp   =   " " +   chPrefix + 
                    " " +   STRING( chAttribute, 
                                    "X(" + STRING(MAX(iAttrWidth, LENGTH(chAttribute) + 2)) + ")"
                                    )
        .

RETURN(chTmp). 
END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GenerateAttribute(  iAttribute  AS INTEGER,  
                                                            iAttrWidth  AS INTEGER,
                                                            chPrefix    AS CHARACTER
                                                            ):
/****************************************************************************/
/* Generate a space-padded numeric attribute - this is used to get an       */
/* standard sized attribute which is used to column-align a set of code     */
/* elements.                                                                */
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chTmp                   AS CHARACTER NO-UNDO.

/****************************************************************************/

IF iAttribute > 0 THEN 
    ASSIGN 
        chTmp   =   " " + chPrefix + 
                    " " + STRING(   STRING(iAttribute), 
                                    "X(" + STRING(iAttrWidth) + ")"
                                ) 
        . 

RETURN(chTmp). 
END METHOD. 

/***************************************************************************/
METHOD PUBLIC CHARACTER                 GenerateCommentTableHasNoFields():
/****************************************************************************/
/* Return a standard string which indicates that a table has no fields      */
/***************************************************************************/
RETURN(THIS-OBJECT:GenerateComment(SUBSTITUTE("Table &1 has no fields.", THIS-OBJECT:TableName))). 
END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GenerateComment(chCommentText AS CHARACTER):
/****************************************************************************/
/* Return a standard string which embeds a comment in comment code          */
/****************************************************************************/
RETURN("~/~* " + chCommentText + "~*~/"). 
END METHOD.
 
/****************************************************************************/
METHOD PUBLIC LOGICAL                   CheckIfThisTableHasAnyFields(riParm AS RECID):
/****************************************************************************/
/* Standard check to see if a table as at least one field in it             */    
/****************************************************************************/
/* Local Buffer                         */

DEFINE BUFFER DbTable                   FOR dictdb._File.
DEFINE BUFFER TableField                FOR dictdb._Field.

/****************************************************************************/
RETURN(CAN-FIND(FIRST TableField WHERE TableField._file-recid = riParm)).
END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GetGlobalDefineHeader(      chPrefix AS CHARACTER, 
                                                                    chSuffix AS CHARACTER, 
                                                                    chExtension AS CHARACTER
                                                                    ):
/****************************************************************************/
/* Return a standard sentinel &IF / &GLOBAL DEFINE statement for a file     */
/* name and a define name                                                   */    
/****************************************************************************/
/* Local Variables                      */

DEFINE VARIABLE chFileName              AS CHARACTER    NO-UNDO.
DEFINE VARIABLE chGlobalDefine          AS CHARACTER    NO-UNDO.
DEFINE VARIABLE chRetVal                AS CHARACTER    NO-UNDO.

/****************************************************************************/
ASSIGN 
    chFileName = THIS-OBJECT:GetOffsetTablePathFileName(chPrefix, 
                                                        chSuffix, 
                                                        chExtension
                                                        )
    .

ASSIGN 
    chGlobalDefine = REPLACE(chFileName,        THIS-OBJECT:GenerateSupport:DirectorySeperator, "-")
    chGlobalDefine = REPLACE(chGlobalDefine,    ".",                                            "-")
    chGlobalDefine = chGlobalDefine + "__" 
    .
    
ASSIGN 
    chRetVal = SUBSTITUTE("&2IF DEFINED(&1) = 0 &2THEN",    chGlobalDefine, "&")  + CHR(10) + 
               SUBSTITUTE("&2GLOBAL-DEFINE &1",             chGlobalDefine, "&") 
    .

RETURN(chRetVal).
END METHOD.
 
/****************************************************************************/
METHOD PUBLIC CHARACTER                 GetFullTablePathFileName(   chPrefix AS CHARACTER, 
                                                                    chSuffix AS CHARACTER, 
                                                                    chExtension AS CHARACTER
                                                                    ):
/****************************************************************************/
/* Ths is used to convert the current table name, a name prefix, and a name */
/* suffix into a full path to a file name. This is used to construct the    */
/* end-file name for generated code.                                        */ 
/****************************************************************************/
RETURN(THIS-OBJECT:GenerateSupport:CombineDirectoryPathList(THIS-OBJECT:FullTablePath   + "," + 
                                                            chPrefix                    + 
                                                            THIS-OBJECT:TableName       + chsuffix + 
                                                            IF chExtension > "" 
                                                                THEN "." + chExtension 
                                                                ELSE ""
                                                            )
    ). 
END METHOD. 

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GetOffsetTablePathFileName( chPrefix AS CHARACTER, 
                                                                    chSuffix AS CHARACTER, 
                                                                    chExtension AS CHARACTER
                                                                    ):
/****************************************************************************/
/* Ths is used to convert the current table name, a name prefix, and a name */
/* suffix into a partial path to a file name. This is used to construct the */
/* end-file name for generated code offset from a base directory            */ 
/****************************************************************************/
RETURN(THIS-OBJECT:GenerateSupport:CombineDirectoryPathList(THIS-OBJECT:OffsetTablePath + "," + 
                                                            chPrefix                    + 
                                                            THIS-OBJECT:TableName       + chsuffix + 
                                                            IF chExtension > "" 
                                                                THEN "." + chExtension 
                                                                ELSE ""
                                                            )
    ). 
END METHOD.

/****************************************************************************/
METHOD PUBLIC CHARACTER                 GetFullTablePath():
/****************************************************************************/
RETURN(THIS-OBJECT:GenerateSupport:CombineDirectoryPathList(THIS-OBJECT:GenerateSupport:FullDirectoryPath + "," + 
                                                            THIS-OBJECT:TableName
                                                            )
        ).

END METHOD.
END CLASS.