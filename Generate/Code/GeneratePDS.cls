/*------------------------------------------------------------------------
File        :   Generate.Code.GeneratePDS
Purpose     :   Generate code which defines a single-TT PDS for a given db table  

Author(s)   :   Tim Kuehn, TDK Consulting Services Inc  
Copyright   :   2016-2017 TDK Consulting Services Inc  

    This file is part of TDK Consulting's OOABL Schema Code Generator.

    The OOABL Schema Code Generator is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    The OOABL Schema Code Generator is distributed in the hope that it 
    will be useful, but WITHOUT ANY WARRANTY; without even the implied 
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OOABL Schema Code Generator.  
    If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------*/

/****************************************************************************/
/* Class Using                          */

USING Generate.Code.*                   FROM PROPATH.

/****************************************************************************/
/* Error Handling                       */		

BLOCK-LEVEL ON ERROR UNDO, THROW.

/****************************************************************************/
CLASS Generate.Code.GeneratePDS
    INHERITS GenerateBase
    : 

/****************************************************************************/
/* Class Buffer                         */

DEFINE BUFFER DbTable                   FOR DICTDB._file.

/****************************************************************************/
CONSTRUCTOR                             GeneratePDS(oGenSupport     AS GenerateSupport,
                                                    chTableName         AS CHARACTER
                                                    ):
/****************************************************************************/
/* Configure GenerateBase for generating a PDS definition                   */
/****************************************************************************/
SUPER(  oGenSupport,        /* Support code instance                */ 
        chTableName,        /* Name of the table to generate for    */ 
        "pds"               /* Generated code file extension        */
        ).
 
FIND DbTable
    WHERE DbTable._file-name = chTableName
    NO-LOCK
    NO-ERROR
    .

END CONSTRUCTOR.

/****************************************************************************/
METHOD PUBLIC VOID                      GeneratePDSFile():
/****************************************************************************/
/* API for generating the PDS definition                                    */
/****************************************************************************/

    /* Output to file named "pds" + TableName + ".pds"  */

OUTPUT 
    TO VALUE(THIS-OBJECT:GetFullTablePathFileName("pds", "", "pds"))
    .

    /* If this db table has fields, then generate code          */

     
IF THIS-OBJECT:CheckIfThisTableHasAnyFields(RECID(DbTable)) THEN 
    DO:    
    THIS-OBJECT:GenerateHeader().
    THIS-OBJECT:GenerateTableReference().
    THIS-OBJECT:GenerateFooter().
    END.
ELSE        /* IF this table does not have fields, then         */ 
    DO:     /* generate a "this table has no fields" comment    */
    PUT UNFORMATTED
        THIS-OBJECT:GenerateCommentTableHasNoFields() SKIP   
        .
    END.

    /* All done!    */

OUTPUT CLOSE.

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateHeader():
/****************************************************************************/

PUT UNFORMATTED
        
        /*  Write the top part of the sentinel &GLOBAL-DEFINE   */
        
    THIS-OBJECT:GetGlobalDefineHeader("pds", "", "pds")                     SKIP(1)
        
        /* The include file with the TT specification           */
         
    "~{" + THIS-OBJECT:GetOffsetTablePathFileName("tt", "", "tt") + "~}"    SKIP(1)
    
        /* The dataset name                                     */

    "DEFINE DATASET ds" + THIS-OBJECT:TableName                             SKIP 
    . 

END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateTableReference():
/****************************************************************************/
/* Writes the table reference for the PDS definition indented one tab stop  */    
/****************************************************************************/
    
    /* Add the table name to the specification  */
    
PUT UNFORMATTED
    SPACE(THIS-OBJECT:GenerateSupport:TabSize) 
    "FOR tt" + THIS-OBJECT:TableName    SKIP 
    .
    
END METHOD.

/****************************************************************************/
METHOD PRIVATE VOID                     GenerateFooter():
/****************************************************************************/
/* Finish the definition by adding another line, the statement ending       */
/* period, and the end of the sentinel &GLOBAL-DEFINE                       */    
/****************************************************************************/

PUT UNFORMATTED                                     SKIP 
    SPACE(THIS-OBJECT:GenerateSupport:TabSize) "."  SKIP(1)
    "&ENDIF"                                        SKIP 
    .

END METHOD.

END CLASS.