# README #
### What is this repository for? ###

This ABL code will read all tables in an identified connected database and generate a set of source code files based on the database table definitions it finds. 

This code repository accompanies Tim Kuehn's [PUG Challenge Americas 2017](http://www.pugchallenge.org/) talk "An OO Code Generator".

### What Files Does it Create? 
The files it creates is

* A domain instance class with properties that reflect the table's fields.
* A db mapping class with properties that copy value changes to / from a table's fields
* A TT mapping class with properties that copy value changes to / from a TT's fields
* An interface class for the TT and DB mapping classes
* A data-source declaration for the table 
* A TT declaration that matches the db table structure 

### What Does it Need?
The project was developed in part to demonstrate OO modeling and implementation techniques while generating code that can be used in a production environment. Later versions could include code to 

* Manipulate the buffer inside the db/TT mapping class, 
* Copy data between a domain object and a mapping class instance, 
* Add functionality to support modeling subsets of table fields
* Add functionality to model a multi-table dataset 

### Version
This is an initial version which generates the identified files. Future versions will depend on community input.

### How do I get set up? 

To use this code

1. Download the repository
2. Create an environment with a db connection
3. Follow the instructions in Run/GenerateSchemaCode.p
4. Examine the resulting source code
5. Enjoy!

### License ###
This code in this repository is licensed under General Public License version 3 or later. 

### Who do I talk to? ###
Tim Kuehn - tim.kuehn@tdkcs.com